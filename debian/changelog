hgnested (0.8-1) unstable; urgency=medium

  * Updating signing-key.asc with the actual upstream maintainer keys.
  * Merging upstream version 0.8.
  * Updating to Standards-Version 3.9.8, no changes needed.
  * Running wrap-and-sort -sta.
  * Depend on mercurial >= 3.6 (depend on mercurial from jessie-
    backports).

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 12 Aug 2016 17:07:29 +0200

hgnested (0.7-2) unstable; urgency=medium

  * Adding signature check, release tarballs are now signed.
  * Updating to standards version 3.9.7, no changes needed.
  * Updating VCS-Browser to https and canonical cgit URL.
  * Updating VCS-Git to https and canonical cgit URL.
  * Removing the braces from the dh call.
  * Removing the version constraint from python.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 08 Mar 2016 18:54:02 +0100

hgnested (0.7-1) unstable; urgency=medium

  * Correcting package long description.
  * Merging upstream version 0.7.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 03 Sep 2015 13:37:12 +0200

hgnested (0.6-7) unstable; urgency=medium

  * Adding actual upstream signing key.
  * Using pypi.debian.net redirector in watch file.
  * Wrapping and sorting control files (wrap-and-sort -bts).
  * Updating year of debian copyright.
  * New home of hgnested.
  * Adapting section naming in gbp.conf to current git-buildpackage.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 30 Jun 2015 11:50:16 +0200

hgnested (0.6-6) unstable; urgency=medium

  * Removing  LC_ALL=C.UTF-8 as build environment.
  * Removing accidently doubled option pgpsigurlmangle from watch file.
  * Updating signing key while using now plain .asc files instead of .pgp
    binaries.
  * Updating gbp.conf for the usage of the tarball compression.
  * Adding actual upstream signing key.
  * Updating to Standards-Version: 3.9.6, no changes needed.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 08 Oct 2014 19:19:09 +0200

hgnested (0.6-5) unstable; urgency=medium

  * Updating year in debian copyright.
  * Removing debian/source/options, we are building with dpkg defaults.
  * Removing PYBUILD_DESTDIR_python2 from rules, it is no more needed.
  * Adding pgp verification for uscan.
  * Adding gbp.conf for usage with git-buildpackage.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 11 Mar 2014 17:13:01 +0100

hgnested (0.6-4) unstable; urgency=low

  * Updating to standards version 3.9.5, no changes needed.
  * Changing to buildsystem pybuild.
  * Pointing VCS fields to new location on alioth.debian.org.
  * Using dpkg defaults for xz compression.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 29 Nov 2013 19:03:57 +0100

hgnested (0.6-3) unstable; urgency=low

  * Adapting the rules file to work also with git-buildpackage.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 06 Aug 2013 13:31:52 +0200

hgnested (0.6-2) unstable; urgency=low

  * Removing Daniel from Uploaders. Thanks for your work! (Closes: #704333).
  * Removing needless empty line in rules.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 31 May 2013 15:11:51 +0200

hgnested (0.6-1) experimental; urgency=low

  * Removing obsolete Dm-Upload-Allowed
  * Updating to Standards-Version: 3.9.4, no changes needed.
  * Updating Vcs-Git to correct address.
  * Adding watch file.
  * Merging upstream version 0.6.
  * Updating copyright.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sun, 24 Feb 2013 14:21:57 +0100

hgnested (0.5-3) unstable; urgency=low

  * Correcting email address in previous upload.

 -- Daniel Baumann <daniel@debian.org>  Tue, 10 Jul 2012 23:18:28 +0200

hgnested (0.5-2) unstable; urgency=low

  * Updating maintainers field.
  * Updating vcs fields.
  * Updating to debhelper version 9.
  * Switching to xz compression.
  * Correcting copyright file to match format version 1.0.

 -- Daniel Baumann <daniel@debian.org>  Sat, 30 Jun 2012 17:13:05 +0200

hgnested (0.5-1) unstable; urgency=low

  * Updating to Standards-Version: 3.9.3, no changes needed.
  * Updating year in copyright.
  * Adding Format header for DEP5.
  * Merging upstream version 0.5.
  * Updating year in Copyright.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 30 Apr 2012 18:30:12 +0200

hgnested (0.4-1) unstable; urgency=low

  * Merging upstream version 0.4.
  * Removing deprecated XB-Python-Version for dh_python2.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sun, 14 Aug 2011 12:43:59 +0200

hgnested (0.3-2) unstable; urgency=low

  [ Mathias Behrle ]
  * Removing wrong recommend for python-suds.
  * Updating to standards version 3.9.2.
  * Removing default activation of the extension.

  [ Daniel Baumann ]
  * Removing cruf changelog file.
  * Compacting copyright file.
  * Moving local config file to local subdirectory for better order.
  * Moving to source format 3.0 (quilt).

  [ Mathias Behrle ]
  * Moving from deprecated python-support to dh_python2.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sun, 17 Jul 2011 19:35:27 +0200

hgnested (0.3-1) unstable; urgency=low

  * Updating Copyright.
  * Adding configuration file to enable extension at installation time.
  * Merging upstream version 0.3.
  * Updating Copyright.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sun, 27 Feb 2011 13:25:10 +0100

hgnested (0.2-2) unstable; urgency=low

  * Switching package section from python to vcs (Closes: #603241).

 -- Daniel Baumann <daniel@debian.org>  Fri, 12 Nov 2010 12:37:17 +0100

hgnested (0.2-1) unstable; urgency=low

  * Initial release.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sun, 31 Oct 2010 18:37:12 +0100
